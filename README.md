# WTLFD

Where to live Fuzzy decision maker

## The data

The data were obtained from The World Bank using [this filter](http://databank.worldbank.org/data/reports.aspx?Report_Name=Choosing-country-to-live-in&Id=38a3f428) for all available countries.

# Installation & the launch

```bash
# Install pip for python3 & git
apt-get install python3-pip git

# Install scikit-fuzzy package for python3
pip3 install scikit-fuzzy

# Clone the project
git clone https://gitlab.com/jirislav/WTLFD.git

# Move to the project directory and run the Where to live Fuzzy decision maker!
cd WTLFD
./wtlfd.py
```

#!/usr/bin/env python3
"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 17.4.18
"""

from lib.controller import FuzzyController
from lib import formatter, loader, interact, parser


def parse_data() -> dict:
    data = loader.load_csv(loader.CSV_DATA)
    data_bank_parser = parser.DataBankCsvParser(data)
    return data_bank_parser.as_dict()


def get_definition() -> list:
    return loader.load_csv(loader.CSV_DEFINITION_AND_SOURCE)


def get_controller() -> FuzzyController:
    inputs, weights = interact.get_weighted_inputs()
    data = parse_data()

    return FuzzyController(
        inputs=inputs,
        weights=weights,
        data=data
    )


def run():
    interact.welcome()

    ctrl = get_controller()

    result = ctrl.compute()

    interact.format_result(ctrl.inputs, ctrl.weights, result)

    interact.prompt_exit()


if __name__ == '__main__':
    try:
        run()
    except KeyboardInterrupt:
        print(formatter.heading("See you next time, bye! :)"))
        from sys import exit
        exit(1)
    except Exception:
        print(formatter.bold_heading("Oh no! Something bad happened ! :("))
        raise

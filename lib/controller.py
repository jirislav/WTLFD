"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 17.4.18
"""
from collections import OrderedDict
from typing import List, Dict, Union, Any

import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

from lib.rules import *


class FuzzyController(object):
    CONSEQUENTS_STEPS = 100

    antecedents = {}
    consequents = {}

    rules = []

    def __init__(self, inputs: dict, weights: dict, data: dict):
        """
            This fuzzy controller accepts inputs & the data as the input

        :param inputs:
        :param weights:
        :param data:
        """
        self.inputs = inputs
        self.weights = weights
        self.data = data

        # We will only compute from the filled ancendents ...
        self._set_of_filled_ancendents = set(self.inputs.keys())

        # Determine sensible consequents ...
        self._set_of_sensible_consequents = FuzzyController.get_sensible_consequents(
            self._set_of_filled_ancendents
        )

        self._create_antecedents()
        self._create_consequents()
        self._create_rules()

        self.control_system = ctrl.ControlSystem(self.rules)

    def compute(self) -> List[Dict[str, Union[int, Any]]]:
        """
            Returns the fuzzy output of this controller

        :return:
        """
        result = ctrl.ControlSystemSimulation(self.control_system)
        result.inputs(self.inputs)
        result.compute()

        consequent_scores = {}
        for consequent_name, consequent in self.consequents.items():

            # Uncomment this to see graphs of consequents results ...
            # consequent.view(sim=result)

            boundaries = self.data['boundaries'][consequent_name]
            universe_size = boundaries['max']['value'] - boundaries['min']['value']

            best_value = result.output[consequent_name]

            for country_code, country in self.data['data'].items():

                country_value = country[consequent_name]

                if country_value is None:
                    # The value for this country is not available :/
                    continue

                if country_code not in consequent_scores:
                    consequent_scores[country_code] = {}

                # The best matching countries will have the lowest scores :)
                # - you can also image that country with the closest value will have the best score ..
                #
                # We also have to divide the value by the universe size so that all dimensions are comparable
                consequent_scores[country_code][consequent_name] = \
                    abs(best_value - country_value) * 100.0 / universe_size

        if not consequent_scores:
            raise Exception("Failed to compute the scores!")

        weighted_consequent_scores = {}
        for country_code, consequent_score in consequent_scores.items():

            weighted_consequent_scores[country_code] = {}

            for antecedent_name, weight in self.weights.items():

                if weight == 0:
                    # Not a subject of our interest ..
                    continue

                sensible_consequents = MAP_OF_SENSIBLE_CONSEQUENTS[antecedent_name]
                if not sensible_consequents:
                    raise Exception("No sensible consequents exist for antecedent '{antecedent}' !".format(
                        antecedent=antecedent_name
                    ))

                existing_consequents_count = 0
                for consequent_name in sensible_consequents:
                    if consequent_name in consequent_score:
                        existing_consequents_count += 1

                if existing_consequents_count is 0:
                    # We actually have no information about derived consequent belonging to the current country :/
                    # Because user gave this input non-zero weight, we can't count this country because of lacking data
                    del weighted_consequent_scores[country_code]
                    break

                # We have to distribute the weight across all the consequents so that the weight is comparable
                # to all other consequents
                distributed_weight = weight / len(sensible_consequents)

                for consequent_name in sensible_consequents:

                    if consequent_name not in self.consequents.keys():
                        raise Exception(
                            "Missing consequent '{cons}' derived from '{ante}' with non-zero weight '{w}'".format(
                                cons=consequent_name,
                                ante=antecedent_name,
                                w=weight
                            )
                        )

                    if consequent_name not in consequent_score:
                        # Skipping this consequent should be okay thanks to the distributed weight ;)
                        continue

                    # Apply the specified weight to each consequent corresponding to the antecedent of the weight
                    weighted_consequent_scores[country_code][consequent_name] = \
                        consequent_score[consequent_name] * distributed_weight

        if not weighted_consequent_scores:
            raise Exception("Failed to compute the weighted consequent scores!")

        weighted_scores = {}
        for country_code, consequent_score in weighted_consequent_scores.items():
            # Just sum all country specs scores together (the one with the lowest score will be the winner)
            values = consequent_score.values()
            weighted_scores[country_code] = sum(values) / len(values)

        sorted_weighted_scores = sorted(
            (
                (country_code, country_score)
                for country_code, country_score
                in weighted_scores.items()
            ),
            key=lambda weighted_score: weighted_score[1]
        )

        best_countries = [{
            'country_code': country_code,
            'country_name': self.data['data'][country_code]['name'],
            'score': country_score
        } for country_code, country_score in sorted_weighted_scores]

        return best_countries[0:20]

    def _create_antecedents(self) -> None:
        """
            Creates fuzzy inputs

        :return:
        """

        for ancendent_name in self._set_of_filled_ancendents:
            # Every antecedent ranges from 0 to 10 ..
            self.antecedents[ancendent_name] = ctrl.Antecedent(
                np.arange(0, 11, 1),
                ancendent_name
            )

            # Auto-membership function population
            self.antecedents[ancendent_name].automf(3)

        if not self.antecedents:
            raise Exception("No antecedents created!")

    def _create_consequents(self) -> None:
        """
            Creates fuzzy outputs

        :return:
        """

        for consequent_name in self._set_of_sensible_consequents:
            consequent_boundaries = self.data['boundaries'][consequent_name]

            minimum = consequent_boundaries['min']['value']
            maximum = consequent_boundaries['max']['value']

            def calculate_mf_from_percentages(percentages: list):
                return [(maximum - minimum) * percentage + minimum for percentage in percentages]

            # Each consequent can have it's own rules for importancy of certain values
            # This effectively allows us to configure the triangulation member function for each consequent in globals
            low = IMPORTANCY_OF_CONSEQUENTS[consequent_name]['low']
            mid = IMPORTANCY_OF_CONSEQUENTS[consequent_name]['mid']
            high = IMPORTANCY_OF_CONSEQUENTS[consequent_name]['high']

            # Now let's dynamically create the mf using provided percentages
            membership_functions = {
                'low': calculate_mf_from_percentages(low),
                'mid': calculate_mf_from_percentages(mid),
                'high': calculate_mf_from_percentages(high)
            }

            # Define boundaries of the universe
            #  - can be actually outside of the real values, but only to be able to obtain the lowest/highest value
            #    using the centroid function
            universe_start = min((min(membership_functions[mf]) for mf in ('low', 'mid', 'high')))
            universe_stop = max((max(membership_functions[mf]) for mf in ('low', 'mid', 'high')))

            # Create the consequent according to the boundaries of corresponding variable ..
            consequent = ctrl.Consequent(
                universe=np.linspace(
                    start=universe_start,
                    stop=universe_stop,
                    num=self.CONSEQUENTS_STEPS
                ),
                label=consequent_name
            )

            consequent['low'] = fuzz.trimf(
                consequent.universe,
                membership_functions['low']
            )

            consequent['mid'] = fuzz.trimf(
                consequent.universe,
                membership_functions['mid']
            )

            consequent['high'] = fuzz.trimf(
                consequent.universe,
                membership_functions['high']
            )

            self.consequents[consequent_name] = consequent

        if not self.consequents:
            raise Exception("No consequents created!")

    def _create_rules(self) -> None:
        """
            Creates fuzzy rules to map consequents from ancendents

        :return:
        """
        for consequent_name in self._set_of_sensible_consequents:
            for level in ('low', 'mid', 'high'):
                self.rules.append(
                    ctrl.Rule(
                        RULES_OF_CONSEQUENTS[consequent_name](self.antecedents, level),
                        self.consequents[consequent_name][level]
                    )
                )

        if not self.rules:
            raise Exception("No rules created!")

    @staticmethod
    def get_sensible_consequents(input_ancendents: set) -> set:
        """
            Returns new set with consequents that are to be expected on output of the fuzzy system if provided
            ancendents are going to represent the inputs.

        :param input_ancendents:
        :return:
        """
        if not input_ancendents:
            raise Exception("Input ancendents are empty, so I have nothing to work with!")

        sensible_consequents = set()
        for ancendent in input_ancendents:
            sensible_consequents |= MAP_OF_SENSIBLE_CONSEQUENTS[ancendent]

        if not sensible_consequents:
            raise Exception("There are missing sensible consequents for the following ancendents: {}".format(
                input_ancendents
            ))

        return sensible_consequents

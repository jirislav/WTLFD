"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 21.4.18
"""


def heading(message: str, decorator_char: str = '-') -> str:
    # Make sure we have only one char ..
    decorator_char = decorator_char[0]

    # Prepare decorator line ..
    decorator_line = decorator_char * (len(message) + 3 * 2)
    decorator_line = decorator_line.join(('\n', '\n'))

    # Return the result ..
    return message.join(
        (
            '{}  '.format(decorator_char),
            '  {}'.format(decorator_char)
        )
    ).join(
        (
            '\n' + decorator_line,
            decorator_line
        )
    )


def bold_heading(message: str) -> str:
    return heading(
        message=message,
        decorator_char='='
    )

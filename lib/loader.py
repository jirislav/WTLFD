"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 17.4.18
"""

import csv

DATA_DIR = 'data'

CSV_DATA = 'Data.csv'
CSV_DEFINITION_AND_SOURCE = 'Definition and Source.csv'


def load_csv(name: str) -> list:
    with open('{}/{}'.format(DATA_DIR, name), 'r') as csvfile:
        data = []
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(row)

        return data

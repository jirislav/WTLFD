"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 17.4.18
"""
import logging

from typing import Union
from lib.glob import SERIES_CODE_TO_CONSEQUENTS_MAPPING

CSV_COUNTRY_NAME_COL = 0
CSV_COUNTRY_CODE_COL = 1
CSV_SERIES_DESC_COL = 2
CSV_SERIES_CODE_COL = 3
CSV_VALUE_COL = 4


class DataBankCsvParser(object):
    header = None

    data = {}
    sorted = {}
    boundaries = {}

    def __init__(self, loaded_csv: list):
        """
            Creates instance of the DataBankCsvParser

        :param loaded_csv:
        """
        self._csv = loaded_csv
        self._parse()
        self._sort_by_consequent()
        self._calculate_boundaries()

    @staticmethod
    def __parse_value(str_value, expecting_problem=False) -> Union[int, float, None, bool]:
        """
            Parses the value as defined from databank (can be None, int or float)

            Returns False if there is something else than a value. (empty string is also treated as empty value)

        :param str_value:
        :return:
        """

        # Set value to null if it contains predefined value
        if str_value in ('..', 'Blank', 'NA', '#N/A', ''):
            value = None
        else:
            try:
                if '.' in str_value:
                    # Cast to float if possible
                    value = float(str_value)
                else:
                    # No period? Probably integer
                    value = int(str_value)
            except ValueError:
                # Oops, there's some error
                if not expecting_problem:
                    logging.warning(
                        "Unable to cast '{}' to float nor int! (Setting it to a False value)".format(str_value))
                value = False

        return value

    def _parse(self) -> None:
        """
            Iterates over the CSV and aggregates the data according to the state they belong to

        :return:
        """
        for row in self._csv:

            str_value = row[CSV_VALUE_COL]

            # First line should be header
            if self.header is None:

                # Parse the value first
                value = self.__parse_value(str_value, expecting_problem=True)

                if value is False:
                    # Unless a column name is float, int or empty, we should be certain that there is header ..
                    self.header = row
                    continue

                # This csv doesn't contain header ..
                self.header = False
            else:
                # Parse the value regularly ..
                value = self.__parse_value(str_value)

            # Get other corresponding values
            country_name = row[CSV_COUNTRY_NAME_COL]
            country_code = row[CSV_COUNTRY_CODE_COL]
            series_desc = row[CSV_SERIES_DESC_COL]  # unused ..
            series_code = row[CSV_SERIES_CODE_COL]

            # Some invalid line :/
            if country_code == '':
                continue

            # Create the dict for current country if not created already
            if country_code not in self.data:
                self.data[country_code] = {
                    'name': country_name
                }

            # Translate the variable code into sensible variable name - fallback to the code itself
            mapped_consequent = SERIES_CODE_TO_CONSEQUENTS_MAPPING.get(series_code, series_code)

            # Then write it to the data
            self.data[country_code][mapped_consequent] = value

    def _sort_by_consequent(self) -> None:
        for consequent_name in SERIES_CODE_TO_CONSEQUENTS_MAPPING.values():
            sorted_data = sorted(
                (
                    (country_code, country_info)
                    for country_code, country_info
                    in self.data.items()
                    if country_info.get(consequent_name) is not None
                ),
                key=lambda country_info: country_info[1][consequent_name]
            )

            self.sorted[consequent_name] = [{
                'country_code': country_code,
                'country_name': country_info['name'],
                'value': country_info[consequent_name]
            } for country_code, country_info in sorted_data]

    def _calculate_boundaries(self) -> None:
        for variable, sort_list in self.sorted.items():
            bottom_country = sort_list[0]
            top_country = sort_list[-1]

            self.boundaries[variable] = {
                'min': bottom_country,
                'max': top_country
            }

    def as_dict(self):
        return {
            'header': self.header,
            'data': self.data,
            'sorted_vars_asc': self.sorted,
            'boundaries': self.boundaries
        }

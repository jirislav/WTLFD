"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 22.4.18
"""

from lib.glob import *


def RULE_OF_DRINKING_WATER_COVERAGE(antecedents: dict, level: str):
    life_expectancy_specified = ANTECEDENT_NAME_LIFE_EXPECTANCY in antecedents
    global_health_specified = ANTECEDENT_NAME_GLOBAL_HEALTH in antecedents

    if life_expectancy_specified and global_health_specified:

        life_expectancy_antecedent = antecedents[ANTECEDENT_NAME_LIFE_EXPECTANCY]
        global_health_antecedent = antecedents[ANTECEDENT_NAME_GLOBAL_HEALTH]

        if level == 'low':
            return \
                life_expectancy_antecedent['poor'] \
                | global_health_antecedent['poor']

        elif level == 'mid':
            return \
                global_health_antecedent['average'] \
                | life_expectancy_antecedent['average']

        else:
            return \
                life_expectancy_antecedent['good'] \
                | global_health_antecedent['good']

    elif life_expectancy_specified:
        life_expectancy_antecedent = antecedents[ANTECEDENT_NAME_LIFE_EXPECTANCY]

        if level == 'low':
            return life_expectancy_antecedent['poor']

        elif level == 'mid':
            return life_expectancy_antecedent['average']

        else:
            return life_expectancy_antecedent['good']

    elif global_health_specified:
        global_health_antecedent = antecedents[ANTECEDENT_NAME_GLOBAL_HEALTH]

        if level == 'low':
            return global_health_antecedent['poor']

        elif level == 'mid':
            return global_health_antecedent['average']

        else:
            return global_health_antecedent['good']

    raise Exception("Cannot determine drinking water coverage when life_expectancy nor global_health is not set")


def RULE_OF_NEONATAL_MORTALITY_RATE(antecedents: dict, level: str):
    global_health = antecedents.get(ANTECEDENT_NAME_GLOBAL_HEALTH, None)

    if global_health is None:
        raise Exception("Cannot determine neonatal mortality rate when global_health is not set!")

    if level == 'low':
        return global_health['good']
    elif level == 'mid':
        return global_health['average']
    else:
        return global_health['poor']


def RULE_OF_LIFE_EXPECTANCY_YEARS(antecedents: dict, level: str):
    life_expectancy = antecedents.get(ANTECEDENT_NAME_LIFE_EXPECTANCY, None)

    if life_expectancy is None:
        raise Exception("Cannot determine life expectancy!")

    if level == 'low':
        return life_expectancy['poor']
    elif level == 'mid':
        return life_expectancy['average']
    elif level == 'high':
        return life_expectancy['good']


def RULE_OF_UNEMPLOYMENT(antecedents: dict, level: str):
    unemployment = antecedents.get(ANTECEDENT_NAME_UNEMPLOYMENT, None)

    if unemployment is None:
        raise Exception("Cannot determine unemployment!")

    if level == 'low':
        return unemployment['poor']
    elif level == 'mid':
        return unemployment['average']
    elif level == 'high':
        return unemployment['good']


def RULE_OF_INFRASTRUCTURE_QUALITY(antecedents: dict, level: str):
    infrastructure_quality = antecedents.get(ANTECEDENT_NAME_INFRASTRUCTURE_QUALITY, None)

    if infrastructure_quality is None:
        raise Exception("Cannot determine infrastructure quality!")

    if level == 'low':
        return infrastructure_quality['poor']
    elif level == 'mid':
        return infrastructure_quality['average']
    elif level == 'high':
        return infrastructure_quality['good']


def RULE_OF_PUPIL_TEACHER_RATIO(antecedents: dict, level: str):
    education_level = antecedents.get(ANTECEDENT_NAME_EDUCATION_LEVEL, None)

    if education_level is None:
        raise Exception("Cannot determine pupil_teacher_ratio without education level!")

    if level == 'low':
        return education_level['poor']
    elif level == 'mid':
        return education_level['average']
    elif level == 'high':
        return education_level['good']


def RULE_OF_EDU_SECONDARY_DONE(antecedents: dict, level: str):
    education_level = antecedents.get(ANTECEDENT_NAME_EDUCATION_LEVEL, None)

    if education_level is None:
        raise Exception("Cannot determine percentage of completed secondary education without education level!")

    if level == 'low':
        return education_level['poor']
    elif level == 'mid':
        return education_level['poor']
    elif level == 'high':
        return education_level['average'] | education_level['good']


def RULE_OF_EDU_TERTIARY_DONE(antecedents: dict, level: str):
    education_level = antecedents.get(ANTECEDENT_NAME_EDUCATION_LEVEL, None)

    if education_level is None:
        raise Exception("Cannot determine percentage of completed tertiary education without education level!")

    if level == 'low':
        return education_level['poor']
    elif level == 'mid':
        return education_level['average']
    elif level == 'high':
        return education_level['good']


def RULE_OF_URBAN_POPULATION(antecedents: dict, level: str):
    urbanization = antecedents.get(ANTECEDENT_NAME_URBANIZATION, None)

    if urbanization is None:
        raise Exception("Cannot determine urbanization")

    if level == 'low':
        return urbanization['poor']
    elif level == 'mid':
        return urbanization['average']
    elif level == 'high':
        return urbanization['good']


def RULE_OF_RURAL_POPULATION(antecedents: dict, level: str):
    urbanization = antecedents.get(ANTECEDENT_NAME_URBANIZATION, None)

    if urbanization is None:
        raise Exception("Cannot determine rural preferences with missing urbanization!")

    if level == 'low':
        return urbanization['good']
    elif level == 'mid':
        return urbanization['average']
    elif level == 'high':
        return urbanization['poor']


def RULE_OF_POPS_DENSITY(antecedents: dict, level: str):
    pops_density = antecedents.get(ANTECEDENT_NAME_POPS_DENSITY, None)

    if pops_density is None:
        raise Exception("Cannot determine population density!")

    if level == 'low':
        return pops_density['poor']
    elif level == 'mid':
        return pops_density['average']
    elif level == 'high':
        return pops_density['good']


def RULE_OF_AIR_POLLUTION(antecedents: dict, level: str):
    infrastructure_quality = antecedents.get(ANTECEDENT_NAME_AIR_POLLUTION, None)

    if infrastructure_quality is None:
        raise Exception("Cannot determine air pollution!")

    if level == 'low':
        return infrastructure_quality['poor']
    elif level == 'mid':
        return infrastructure_quality['average']
    elif level == 'high':
        return infrastructure_quality['good']


RULES_OF_CONSEQUENTS = {
    CONSEQUENT_DRINKING_WATER_COVERAGE_PCT: RULE_OF_DRINKING_WATER_COVERAGE,
    CONSEQUENT_NEONATAL_MORTALITY_RATE_PCT: RULE_OF_NEONATAL_MORTALITY_RATE,
    CONSEQUENT_LIFE_EXPECTANCY_YEARS: RULE_OF_LIFE_EXPECTANCY_YEARS,
    CONSEQUENT_UNEMPLOYMENT_PCT: RULE_OF_UNEMPLOYMENT,
    CONSEQUENT_INFRASTRUCTURE_QUALITY_1_TO_7: RULE_OF_INFRASTRUCTURE_QUALITY,
    CONSEQUENT_PUPIL_TEACHER_RATIO_PCT: RULE_OF_PUPIL_TEACHER_RATIO,
    CONSEQUENT_EDU_SECONDARY_DONE_PCT: RULE_OF_EDU_SECONDARY_DONE,
    CONSEQUENT_EDU_TERTIARY_DONE_PCT: RULE_OF_EDU_TERTIARY_DONE,
    CONSEQUENT_URBAN_POPULATION_PCT: RULE_OF_URBAN_POPULATION,
    CONSEQUENT_RURAL_POPULATION_PCT: RULE_OF_RURAL_POPULATION,
    CONSEQUENT_POPS_DENSITY_PER_SQ_KM: RULE_OF_POPS_DENSITY,
    CONSEQUENT_AIR_POLLUTION_UG_OF_PM25_PER_M3: RULE_OF_AIR_POLLUTION,
}

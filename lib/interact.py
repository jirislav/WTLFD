"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 17.4.18
"""

from pprint import pprint
from typing import Tuple, List, Dict, Union, Any

from lib.glob import *
from lib.formatter import *


def welcome() -> None:
    print((
            heading("Welcome to the 'Where to live fuzzy decision' !") +
            "\n"
            "I hope you'll find this program useful or at least\n"
            "  interesting to experiment with :)\n"
            "\n"
            "\n"
            "Let me ask you some vague questions to feed me\n"
            "  with some data, so that I can crunch them and\n"
            "  show you the best countries you can live in ! :)\n"
            "\n"
    ))


def prompt_exit() -> None:
    input("Press [ENTER] to exit ...")


def format_result(inputs: dict, weights: dict, result: List[Dict[str, Union[int, Any]]]) -> None:
    print(heading("And the result is:"))
    print("\nInputs:\n")
    pprint(inputs)
    print("\nWeights:\n")
    pprint({key: value * 5.0 for key, value in weights.items()})
    print("\nCountries you could live in:\n")

    # We will show the least suitable country first ...
    for i, country in reversed(list(enumerate(result))):
        print(bold_heading("{order}. = {short} ({full}) with {match:.02f} % match !".format(
            order=i + 1,
            short=country['country_code'],
            full=country['country_name'],
            match=100.0 - country['score']
        )))


def ask_float(question: str, lowest_help: str, highest_help: str, min: int = 0, max: int = 10,
              middle_help: str = 'Somewhat between', mid: int = -1) -> float:
    print(heading(question))

    answer = None

    # If provided middle value, just add it to the help
    if mid != -1 and max > mid > min:
        middle_string = (
            ' {mid:3} - {mid_help}\n\n'
            '        ...\n'
            '        ...\n\n'
        ).format(
            mid=mid,
            mid_help=middle_help
        )
    else:
        middle_string = ''

    while answer is None:

        # Print values help ..
        print((
            ' {min:3} - {low_help}\n\n'
            '        ...\n'
            '        ...\n\n'
            '{middle_string}'
            ' {max:3} - {high_help}\n\n'
        ).format(
            min=min,
            mid=mid,
            max=max,
            low_help=lowest_help,
            middle_string=middle_string,
            high_help=highest_help
        ))

        # Ask the question ..
        answer = input(
            'Enter number between {min} - {max} (can be decimal): '.format(
                min=min,
                max=max,
            )
        )

        try:
            # Allow the float to be specified with comma
            answer = float(answer.replace(',', '.'))
        except ValueError:
            print(bold_heading('You didn\'t enter a number!'))
            answer = None
            continue

        if not (max >= answer >= min):
            print(
                bold_heading(
                    'The value must be within {min} - {max}!'.format(
                        min=min,
                        max=max
                    )
                )
            )
            answer = None
            continue

    return answer


def ask_weight(importancy_of_what: str) -> float:
    weight = ask_float(
        question="How important is answering about {} to you?".format(importancy_of_what.upper()),
        lowest_help="Not at all",
        middle_help="Somewhat normal",
        highest_help="This problem really matters to me",
        min=0,
        mid=5,
        max=10
    )

    # We will actually weight from 0 - 2
    #
    # Weight is used to multiply the score, so that's why 0 - 2 with 1 in the middle ..
    return weight / 5.0


def ask_global_health(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_GLOBAL_HEALTH] = ask_weight('healthcare')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_GLOBAL_HEALTH] != 0:
        inputs[ANTECEDENT_NAME_GLOBAL_HEALTH] = ask_float(
            question="What level of healthcare you want in the country?",
            lowest_help="I'm good on my own, even without drinking water",
            highest_help="I want the country to be among the most advanced in healthcare"
        )


def ask_air_pollution(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_AIR_POLLUTION] = ask_weight('air pollution')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_AIR_POLLUTION] != 0:
        inputs[ANTECEDENT_NAME_AIR_POLLUTION] = ask_float(
            question="How much do you tolerate the air pollution?",
            lowest_help="I need the air to be really clean",
            highest_help="I somehow like the air being polluted"
        )


def ask_urbanization(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_URBANIZATION] = ask_weight('urbanization')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_URBANIZATION] != 0:
        inputs[ANTECEDENT_NAME_URBANIZATION] = ask_float(
            question="Would you like most of the nation to live in rural or urban area?",
            lowest_help='I want everyone to live in rural area',
            highest_help='I want everyone to live in urban area'
        )


def ask_pops_density(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_POPS_DENSITY] = ask_weight('population density')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_POPS_DENSITY] != 0:
        inputs[ANTECEDENT_NAME_POPS_DENSITY] = ask_float(
            question="How much crowded country you prefer?",
            lowest_help="I want as much room for myself as possible",
            highest_help="I don't mind if there is much people"
        )


def ask_unemployment(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_UNEMPLOYMENT] = ask_weight('unemployment')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_UNEMPLOYMENT] != 0:
        inputs[ANTECEDENT_NAME_UNEMPLOYMENT] = ask_float(
            question="How much people can be unemployed in the country?",
            lowest_help="Everyone should be working",
            highest_help="No one does need to work, ever"
        )


def ask_education_level(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_EDUCATION_LEVEL] = ask_weight('education level')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_EDUCATION_LEVEL] != 0:
        inputs[ANTECEDENT_NAME_EDUCATION_LEVEL] = ask_float(
            question="What education level you want?",
            lowest_help="Should be really low",
            highest_help="Should be among the best"
        )


def ask_infrastructure_quality(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_INFRASTRUCTURE_QUALITY] = ask_weight('infrastructure quality')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_INFRASTRUCTURE_QUALITY] != 0:
        inputs[ANTECEDENT_NAME_INFRASTRUCTURE_QUALITY] = ask_float(
            question="What infrastructure quality you want?",
            lowest_help="Should be really low",
            highest_help="Should be among the best"
        )


def ask_life_expectancy(inputs: dict, weights: dict) -> None:
    weights[ANTECEDENT_NAME_LIFE_EXPECTANCY] = ask_weight('expected lifespan')

    # Don't ask about this theme as it doesn't matter
    if weights[ANTECEDENT_NAME_LIFE_EXPECTANCY] != 0:
        inputs[ANTECEDENT_NAME_LIFE_EXPECTANCY] = ask_float(
            question="What life expectancy should the country have?",
            lowest_help="Short lifespan",
            highest_help="Long lifespan"
        )


def get_weighted_inputs() -> Tuple[dict, dict]:
    inputs = {}
    weights = {}

    nothing_matters = True

    while nothing_matters:
        ask_global_health(inputs, weights)
        ask_air_pollution(inputs, weights)
        ask_urbanization(inputs, weights)
        ask_pops_density(inputs, weights)
        ask_unemployment(inputs, weights)
        ask_education_level(inputs, weights)
        ask_infrastructure_quality(inputs, weights)
        ask_life_expectancy(inputs, weights)

        if any(weights.values()):
            nothing_matters = False
        else:
            print(
                bold_heading(
                    'Cannot continue when nothing matters to you! Let\'s try it again ...'
                )
            )

    return inputs, weights
